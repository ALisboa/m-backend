from django.db import models

class Game(models.Model):
    name = models.CharField(max_length=100)

    def messages(self):
        st = Message.objects.order_by("id").filter(game=self).all()
        messages = list(st)
        print(len(messages))
        return messages



class Message(models.Model):
    content = models.TextField(default="NONE")
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    timestamp = models.DateField(auto_now_add=True)

    def getOldMessages(self):
        return Message.objects.order_by("-id").all()

    def __str__(self):
        return ' { \'content\': ' + str(self.content) + ', '\
            ' \'timestamp\': ' + str(self.timestamp) + '}'



# Create your models here.
