from django.apps import AppConfig


class SynkConfig(AppConfig):
    name = 'synk'
